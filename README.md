# REST API for CloudFoxy Smartcard Platform

This server is using 
[GlobalPlatformPro](https://github.com/martinpaljak/GlobalPlatformPro)
for smartcard operations and builds a REST interface to provide remote access 
to smartcards, including java card applets.

## Table of Contents
1. [Get Started](#get-started)
2. [Authorization - Must Read](#authorization)
3. [Inventory](#inventory)
   - [hello](#hello-endpoint)
   - [inventory](#inventory-endpoint)
4. [Basic API](#basic-api)
5. [Basic API - JSON](#basic-api---json)
6. [Raw API](#raw-api)
7. [Protocol API](#protocol-api)
    - [Smartcard Identification](#smartcard-identification)
8. [Technical Notes](#technical-notes)
    - [Info](#info)
    - [ToDO](#todo)
    - [Running](#running)
    - [Embedded Container gradle](#embedded-container-gradle)
    - [Jar](#jar)
    - [War](#war)

## Get Started

Please have a look at the [https://gitlab.com/cloudfoxy/FoxyRest/blob/master/OPERATIONS.md](https://gitlab.com/cloudfoxy/RESTfulFoxy/blob/master/OPERATIONS.md) guide for details how to install the server.

Here are main steps that need to be done for correct installation and configuration.

You can download a pre-compiled jar file, together with a CloudFoxy smartcardio
driver. 

The main configuration file for the server is in the appConfig.yml file. It has 
to be in the PATH environment variable, or in the current folder. The main 
configuration options include:
1. enforcement of authorization tokens and values of the tokens - provided as a header "X-Auth-Token";
location of the JKS keystore with the server's HTTPS certificate and key;
2. use of HTTP or HTTPS; and the location of log files.

The server can work with [CloudFoxy hardware](http://cloudfoxy.com)  as well as 
with common smartcard readers. 

A "global.json" configuration file defines where it should search for available 
smart cards. An example of the file content is:

```json
{
    "readers":{
        "use": false,
        "list":["name", "name2"],
        "smartcardio":null
    },
    "simonas":{
        "scan":true,
        "addresses":["192.168.42.10", "192.168.42.18"],
        "smartcardio":"./simonasmartcardio.jar:smarthsmfast.simona.Simonaio:bin%40tcp%3A%2F%2F"
    },
    "appletcheck": "false",
    "protocolfolder":"./protocols",
    "instancefolder":"./instances"
}
```

The "readers" section defines, whether standard smartcard readers should be used,
if yes the "lost" item will list their names. "smartcardio" can remain "null",
unless you need to specify an alternative Smart card IO class.

The "simonas" section lists IP addresses of connected SIMONA boards. The 
smartcardio has to have the value as shown, which consists of the path to the 
JAR file, main class, and parameters.

Additional values must be present but are not important for a general-purpose 
use of the server - the items are needed fo multi-party computation protocols.


## Authorization

All requests must contain the `X-Auth-Token` header with a valid value. Accepted
values are defined in the appConfig.yml file. REST endpoints require one of two
possible roles.

 - management role - this provides access to functions, which change the state 
   of smart-cards, e.g., delete / install / list applets
 - business role - normal operational use of smartcards, i.e., ability to send
   APDU commands, get a list of smartcards, and so on.

## Inventory

The first thing you will probably like to see is whether the server is up and 
running and which smartcards are available. There are two GET RESTful 
endpoints to test this.

*Note: both GET endpoints check the value of "X-Auth-Token" for access control.*

### 'hello' endpoint

This endpoint is at `/api/v1/hello`, e.g.,  https://rest.cloudfoxy.com:8081/api/v1/hello - (with an optional parameter ?name=<name>).

The respoinse is a text of three lines:
1. hello
2. uptime in seconds 
3. number of hello requests

*Example:*
```
Hello, enigma!
uptime 392
counter 1
```

### 'inventory' endpoint

This endpoint is at `/api/v1/inventory/`, e.g., 
https://rest.cloudfoxy.com:8081/api/v1/inventory/ .

This request returns a list of names of readers with ATRs obtained from connected
smartcards.

*Example:*
```
/192.168.42.10@1 3BF81300008131FE454A434F5076323431B7
/192.168.42.10@111 3BF81300008131FE454A434F5076323431B7
```


## Basic API

Basic API is a simplification of the Raw API, which is more suitable for quick 
integrations into applications, and for automation of smartcard use. Unlike the 
Raw API, it does not allow operations with applets - listing, loading, deleting,
or installation but limits operations with smartcards to RESET and IO 
(i.e., sending APDUs).

The basic API endpoint is `/api/v1/basic`, e.g.

https://rest.cloudfoxy.com:8081/api/v1/basic

*Note: Don't forget to set the X-Auth-Token header, if the basic HTTP 
authentication is enabled. The token required is for "business" role.*

The actual command is a hex-encoded string so, e.g., a SELECT command like
`00A404000A4A43416C675465737431` would be requested as:

https://rest.cloudfoxy.com:8081/api/v1/basic?apdu=00A404000A4A43416C675465737431&terminal=%2F192.168.42.10%401   

The response is a one-line text with a hex-encoded response from the smart card. 
When a reset is required, the result will be two lines, with the first being a
result of an initial SELECT command automatically executed after reset.

*Example of response:*

6F048400A5009000
621A82013883023F008404524F4F5485030079AD8A0105A1038B01019000

If we need to request a smartcard reset, we can add an additional parameter 
"reset". The "reset" will be executed before the smartcard command. Using 
the example above, requesting a reset before the SELECT command would look like:

https://rest.cloudfoxy.com:8081/api/v1/basic?reset=1&apdu=00A404000A4A43416C675465737431&terminal=%2F192.168.42.10%401

*Note: the reset will be done automatically, if it is the first use of the 
smart card.*


## Basic API - JSON

The functionality of the Basic API is mirroed in a JSON version, which takes 
exactly same parameters, with the same meaning. The only difference is that the 
result is formatted as a JSON message with more detail of the processing. 

The endpoint is `/api/v1/basicj`, e.g.:

   https:///rest.cloudfoxy.com:8081/api/v1/basicj?reset=1&apdu=00A404000A4A43416C675465737431&terminal=%2F192.168.42.10%401


*Example of a response:*

```json
{
  "status": 0,
  "latency": 12827,
  "error": null,
  "response": {
    "instance": "BASIC-/192.168.42.10@1",
    "detail": {
      "timing": {
        "total": 515,
        "00A4040000": 137,
        "00A40004023F0000": 197
      },
      "result": {
        "00A4040000": "6F048400A5009000",
        "00A40004023F0000": "621A82013883023F008404524F4F5485030079AD8A0105A1038B01019000"
      }
    },
    "size": 1,
    "protocol": "basic"
  }
}
```

## Raw API

The default port of the RESTful API is 8081 (you can change it in the `appConfig.yml`) 
and let's assume it's located at https://rest.cloudfoxy.com:8081

The raw API endpoint is:

https://rest.cloudfoxy.com:8081/api/v1/raw

The protocol is GET and the mandatory parameter is "request", which contains a 
list of standard command line parameters, e.g.,

```-d --list -r "OMNIKEY AG 6121 USB mobile" -v```

```--list --terminals simonasmartcardio.jar:smarthsmfast.simona.Simonaio:bin%40tcp%3A%2F%2F192.168.42.10 -r /192.168.42.10@111 -v```

Parameters mustmust be URL encoded so that the result will look like:

https://rest.cloudfoxy.com:8081/api/v1/raw?request=--list%20--terminals%20simonasmartcardio.jar%3Asmarthsmfast.simona.Simonaio%3Abin%2540tcp%253A%252F%252F192.168.42.10%20-r%20%2F192.168.42.10%40111%20-v
https://rest.cloudfoxy.com:8081/api/v1/raw?request=-d+-list%C2%A0-r+%2F192.168.42.10%401%C2%A0-v

*Note: Don't forget to set the X-Auth-Token header, if the basic HTTP 
authentication is enabled (default). The token required is for "management" role.*

Example of sending a set of APDU commands to a smartcard:

```
00A404000A4A43416C675465737431 - applet selection
0000000000 - command 1

0071000000 - command 2

-a 00A404000A4A43416C675465737431 -a 0000000000 -a 0071000000 -a 0000000000 -r /192.168.42.10@1
```

Response is an ASCII text with a complete output of the GlobalPlatformPro tool like:

```
SCardConnect("/192.168.42.10@11", T=*) -> T=1, 3BF81300008131FE454A434F5076323431B7
SCardBeginTransaction("/192.168.42.10@11")
Reader: /192.168.42.10@11
ATR: 3BF81300008131FE454A434F5076323431B7
More information about your card:
    http://smartcard-atr.appspot.com/parse?ATR=3BF81300008131FE454A434F5076323431B7
A>> T=1 (4+0000) 00A40400 00 
A<< (0102+2) (65ms) 6F648408A000000151000000A5589F6501FF9F6E06479120813B00734906072A864886FC6B01600B06092A864886FC6B020202630906072A864886FC6B03640B06092A864886FC6B040255650B06092B8510864864020103660C060A2B060104012A026E0102 9000
[DEBUG] GlobalPlatform - Auto-detected ISD AID: A000000151000000
[DEBUG] GlobalPlatform - Auto-detected block size: 255
[DEBUG] GlobalPlatform - Lifecycle data (ignored): 0406479120813B00
[DEBUG] GlobalPlatform - Auto-detected GP version: GP22
SCardEndTransaction()
SCardDisconnect("/192.168.42.10@11", true)
```

There are several additional useful endpoints:

`https://rest.cloudfoxy.com:8081/api/v1/mpc/inventory` - this command returns a JSON structure with a list of all detected smart cards, the names can be used to address a particular smartcard when the "raw" endpoint is used.
`https://rest.cloudfoxy.com:8081/api/v1/greeting` - to test the server is listening.




## Protocol API
The server also implements a set of "mpc" endpoints, which can be used to create 
abstractions for sequences of commands. The server configuration must then 
contain JSON definitions of protocols - in the folder identified by the 
"global.json" file.

As this functionality has been implemented for multi-party cryptography (MPC), 
the subpage of this is focussed on describing the use of the API for MPC protocols.

### Smartcard Identification

Smartcards are identified by two different means:
 - ATR
 - lists of applets

Programmable Javacards respond to an initial SELECT command: `00 A4 04 00 00` 
(direct selection by DF, return first record) with a list of installed applets 
and packages. If there's no applet returned, the smartcard will still appear 
in the list of cards with its ATR.

## Technical Notes

### Info

* Provides simple REST binding
* Uses Spring-boot, Spring 4, Gradle

### ToDo

* HTTPS (Letsencrypt)
* Authentication / API key
* Specify allowed commands / cards to use

### Running
There are several ways for testing this REST server. 

### Embedded container - Gradle

Start embedded Tomcat container:

```
./gradlew bootRun
```

Server is reachable on [`http://127.0.0.1:8081`](http://127.0.0.1:8081)

### JAR

To build JAR with embedded servlet container use

```
./gradlew bootRepackage
```

Then run the server

```
java -jar rest/build/libs/gppro-rest-0.1.0.jar
```

### WAR

You can build WAR archive which can be used in the servlet containers such as Tomcat.
To build war:

```
./gradlew war
```



# Installation of FoxyRest

FoxyRest is a RESTful API for remote access to smartcards. Its API is based on 
GlobalPlatformPro.

Installation instructions are described in the [https://gitlab.com/cloudfoxy/FoxyRest/blob/master/OPERATIONS.md](https://gitlab.com/cloudfoxy/FoxyRest/blob/master/OPERATIONS.md#installation).


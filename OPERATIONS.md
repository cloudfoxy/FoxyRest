Any software solution is a complex business and correct operations are important
for a long-term sustainable solution. Cloud Foxy involves hardware components,
which makes it even more important to operate it correctly.

# Overview

The following instructions assume you have either a Cloud Foxy server, or smart 
cards connected to your server with one of the common USB smart-card readers.

These instructions are for Linux distributions and tested against CentOS7.

## Contents

1. [Installation](#installation)
2. [Smartcard Reconfiguration](#smartcard-reconfiguration)
3. [Restart](#restart)
4. [Upgrade](#upgrade)
5. [JSignPDF](#jsignpdf)
6. [Troubleshooting](#troubleshooting)

# Installation

FoxyRest is a Java application, which requires Java installed on the server.
If you haven't installed Java VM yet, go to [https://www.java.com/en/download/](https://www.java.com/en/download/)

## Overview

Installation consists of three main steps:
1. Installing the application itself
2. Update of configuration files
3. Ensuring continuous running of the service


Once Java is correctly installed, you can download the latest jar file from the "binary"
folder.

Create a folder /opt/foxyrest and make it your working directory

Download a zip archive from  [the binary folder](https://gitlab.com/cloudfoxy/FoxyRest/tree/master/binary).


 - `cd /opt/foxyrest`
 - `wget https://gitlab.com/cloudfoxy/FoxyRest/tree/master/binary/latest.zip`
 - `unzip latest.zip`
 
The next step is to make sure that FoxyRest will look for smartcards at the right place. Open the:

`global.json` 

file and update names of smart-card readers and/or IP addresses of CloudFoxy hardware. Check details at
[README.md](https://gitlab.com/cloudfoxy/FoxyRest/blob/master/README.md#get-started).

As an example, let's assume your CloudFoxy crypto provider is at the IP address of *192.168.42.14*. The *global.json* files should look like:

```
{
    "readers":{
        "use": false,
        "list":[],
        "smartcardio":null
    },
    "simonas":{
        "scan":true,
        "addresses":["192.168.42.14"],
        "smartcardio":"./simonasmartcardio.jar:smarthsmfast.simona.Simonaio:bin%40tcp%3A%2F%2F"
    },
    "appletcheck": "false",
    "protocolfolder":"./protocols",
    "instancefolder":"./instances"
}

```

**Other configuration files:**

 - *config/appConfig.xml* - section "logging" defines where will the process store its logs, section "server" defines the port, and http/https use, the "auth/management" section defines authorization tokens that have to be sent in HTTP header "X-Auth-Token".
 - *config/logback.xml* - defines the level of logging 


## Supervisor / Automatic Restarts of FoxyRest

We prefer using **supervisor** to make sure that important servers/services automatically 
restart if there's a problem. We also show how to install and manage FoxyRest with *systemd* in the next section.


### Install supervisor for automatic restarts

`systemctl status supervisord` - if this command shows supervisord is running, 
you can skip to the next section.

`pip install supervisor`

`mkdir -p /etc/supervisord/conf.d` - this is the folder where we define services for
supervisor management

`echo_supervisord_conf > /etc/supervisord/supervisord.conf` - creates a default configuration file, which we need to tweak a little.

`echo [include] >> /etc/supervisord/supervisord.conf`

`echo "files = conf.d/*.conf" >> /etc/supervisord/supervisord.conf`

`nano /etc/supervisord/supervisord.conf`

find a line with _inet_http_server_ and uncomment it (the section name), and the
first line, which is something like "port=127.0.0.1:9001"

`systemctl start supervisord`

`systemctl status supervisord` - this should return a "running" status.

`systemctl enable supervisord`   # auto restart after reboot

### Add FoxyRest to supervisor

Let's assume that the folder for service/unit definitions is: */etc/supervisord/conf.d* as used in the previous section.

We create a configuration file:


`nano /etc/supervisord/conf.d/foxyrest.conf`

and fill it with the following contents
```
[program:foxyrest]
directory=/opt/cloudfoxy/foxyrest
command=start.sh
user=root
autostart=true
autorestart=true
# log files are defined in the FoxyRest configuration file
stderr_logfile=/var/log/foxyrest.log
stdout_logfile=/var/log/foxyrest.log
```

You can adjust parameters as required.

Restart the supervisor:

`systemctl restart supervisord` or use `supervisorctl` to keep other services running

`supervisorctl` - is a client, which shows status of processes - it has commands like:
 - start <name>
 - stop <name>
 - restart <name>
 - reread  # reads configuration files and shows changes
 - reload  # loads the new configuration to use for future commands
 
## Configure FoxyRest for systemd

**Note: choose systemd OR supervisor - do NOT use both at the same time**

The next and optional step is to configure systemd integration. FoxyRest has an
initial configuration file *foxyrest.service*

- `cp foxyrest.service /etc/systemd/system`
- `systemctl enable foxyrest.service`
- `systemctl status foxyrest.service`

If all went well, the FoxyRest service us now up and running.

## Log Redirection

Supervisor running FoxyRest forwards all logging into syslog of that is enabled - which 
is the default case in all Linux distributions. We want to have FoxyRest logs in a separate file.

One of the effects - configuration of logging directories and files elsewhere is
ignored!

First notice that the systemd configuration file foxyrest.service contains 
log redirects to syslog and sets a specific identifier:

```
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=foxyrest
```

This allows us updating the syslog configuration:

```
cd /etc/rsyslog.d/
cp <source>/foxyrest.conf foxyrest.conf
systemctl restart rsyslogd
```


The `foxyrest.conf` file simply redirects lines containing "foxyrest" to a separate
file - `/var/log/foxyrest.log` .



# Smartcard reconfiguration

When you need to change smartcard chips or readers, you have to follow these steps.

## CloudFoxy server

1. Switch-off the server, if there is no main switch, unplug the power cable as well.
2. Open the server's case
3. Unplug the power cable from the CloudFoxy board
4. add/remove smartcard chips as necessary
5. plug the power cable to the CloudFoxy board
6. close the server's case
7. make sure the server's power cable is plugged again
8. switch on the server
9. login to the server with an ssh client and check the FoxyRest is running: `systemctl status foxyrest`
10. if it is not running, start it with `systemctl start foxyrest`

An alternative option - only when you know what you do and you do more extensive 
reconfiguration.

1. Steps 1 and 2 are skipped - server is open but running
2. Follow steps 3-5
3. Make RESTfulFoxy reload new smartcard configuration with an API call /api/v1/resetcards, e.g. `curl --header "X-Auth-Token:<token>" http://127.0.0.1:8081/api/v1/resetcards`

## USB Card Readers

The change here is simpler as you don't need to open the case of your server.

1. Connect / re-connect smartcard readers as necessary
2. restart the foxyrest server, either
   3. `systemctl restart foxyrest`; or
   4. `curl --header "X-Auth-Token:<token>" http://127.0.0.1:8081/api/v1/resetcards`

# Restart

RESTfulFoxy is integrated with systemd so starting, stopping and restarting uses
the `systemctl` command.

`sudo systemctl start foxyrest`

`sudo systemctl status foxyrest`

`sudo systemctl restart foxyrest`

`sudo systemctl stop foxyrest`


# Upgrade

FoxyRest is installed in the `/opt/foxyrest` folder. Upgrade comprises

1. checking the content of the folder `cd /opt/foxyrest/bin; ls -l`
2. stopping the server `sudo systemctl stop foxyrest`
3. download of a new jar file to `/opt/foxyrest/bin`
4. creating a new symlink `rm /opt/foxyrest/bin/foxyrest.jar;` and `ln -s <new jar file> /opt/foxyrest/bin/foxyrest.jar`
5. re-starting the server `sudo systemctl start foxyrest`
 


# JSignPDF


# Troubleshooting

If there are problems, the first source of information is log files. FoxyProxy will have
logs in */var/log/foxyproxy.log* but it's safe to search for "foxy" in */var/log* folder as such.

## Server Processes

Make sure that FoxyRest and FoxyProxy are running. If you use supervisord, that can be simply done with:

`supervisorctl`  -> status, if they are stopped, you can do, e.g., *start foxyrest"


If logs show that proxy doesn't have any reader, a common problem is an incorrect IP address
set in the FoxyRest's *global.json* fil.

Depending on the internal DHCPD server configuration, there may be an incorrect IP address set
for CloudFoxy hardware, just check that IP addresses allocated are in the */opt/foxyrest/global.json* file.

`systemctl status dhcpd`

and

`nano /opt/foxyrest/global.json` -> section simonas->addresses

## CloudFoxy Hardware Check

You can check that the CloudFoxy hardware by connecting to its TCP port. If the
CloudFoxy has a DHCP address *192.168.42.10*, the test would be:

`telnet 192.168.42.10 7746`
  -> disconnect with ctrl+']'
  
  


